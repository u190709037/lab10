package stack;

public class StackItem {

    Object item;
    StackItem next;

    public StackItem(Object item){
        this.item=item;
    }

    public Object accessor(){
        return item;
    }
    public StackItem mutator(){
        return next;
    }


}
